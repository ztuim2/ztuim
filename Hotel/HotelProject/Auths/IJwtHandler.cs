using System;

namespace HotelProject.Auths
{
    public interface IJwtHandler
    {
         string CreateToken(Guid userId, string fullName, string role);
    }
}
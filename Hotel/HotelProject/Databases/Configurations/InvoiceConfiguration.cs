﻿using HotelProject.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotelProject.Databases.Configurations
{
    public class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Reservation)
                .WithOne(x => x.Invoice)
                .HasForeignKey<Invoice>(x => x.ReservationId)
                .OnDelete(DeleteBehavior.Restrict);
            
            builder.HasOne(x => x.Client)
                .WithOne(x => x.Invoice)
                .HasForeignKey<Invoice>(x => x.ClientId)
                .OnDelete(DeleteBehavior.Restrict);
            
            builder.HasOne(x => x.Employee)
                .WithOne(x => x.Invoice)
                .HasForeignKey<Invoice>(x => x.EmployeeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
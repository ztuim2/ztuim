﻿using HotelProject.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HotelProject.Databases.Configurations
{
    public class ReservationConfiguration : IEntityTypeConfiguration<Reservation>
    {
        public void Configure(EntityTypeBuilder<Reservation> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.HasMany(x => x.Rooms)
                .WithOne(x => x.Reservation)
                .HasForeignKey(x => x.ReservationId)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Property(x => x.ReservationStatus)
                .HasConversion<string>();
            
            builder.Property(x => x.PayStatus)
                .HasConversion<string>();
        }
    }
}
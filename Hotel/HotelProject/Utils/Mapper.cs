using System;
using System.Linq;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Repositories;

namespace HotelProject.Utils
{
    public static class Mapper
    {
        public static ClientDto ClientToDto(Client client)
        {
            return new ClientDto
            {
                Id = client.Id,
                FullName = $"{client.FirstName} {client.LastName}",
                Email = client.Email,
                PhoneNumber = client.PhoneNumber
            };
        }
        
        public static EmployeeDto EmployeeToDto(Employee employee)
        {
            return new EmployeeDto
            {
                Id = employee.Id,
                FullName = $"{employee.FirstName} {employee.LastName}",
                Email = employee.Email,
                Role = employee.Role.ToString(),
                Position = employee.Position
            };
        }
        
        public static RoomDto RoomToDto(Room room)
        {
            return new RoomDto
            {
                Id = room.Id,
                Number = room.Number,
                Price = room.Price,
                Type = room.Type.ToString()
            };
        }

        public static ReservationDto ReservationToDto(Reservation reservation, IRoomRepository roomRepository)
        {
            return new ReservationDto
            {
                Id = reservation.Id,
                Description = reservation.Description,
                ClientId = reservation.ClientId,
                ClientName = reservation.ClientName,
                Price = reservation.Price,
                Start = reservation.Start.ToLongDateString(),
                End = reservation.End.ToLongDateString(),
                ReservationStatus = reservation.ReservationStatus.ToString(),
                PayStatus = reservation.PayStatus.ToString(),
                Remarks = reservation.Remarks,
                Rooms = reservation.Rooms.Select(x => HelpWithGetRoom(x.RoomId, roomRepository))
            };
        }

        public static InvoiceDto InvoiceToDto(Invoice invoice)
        {
            return new InvoiceDto
            {
                Id = invoice.Id,
                ClientId = invoice.ClientId,
                Client = invoice.ClientName,
                EmployeeId = invoice.EmployeeId,
                Employee = invoice.EmployeeName,
                ReservationId = invoice.ReservationId,
                Issue = invoice.Issue,
                Pay = invoice.Issue,
                ToPay = invoice.ToPay
            };
        }

        private static RoomDto HelpWithGetRoom(Guid roomId, IRoomRepository roomRepository)
        {
            var room = roomRepository.GetRoom(roomId).GetAwaiter().GetResult();

            return RoomToDto(room);
        }
    }
}
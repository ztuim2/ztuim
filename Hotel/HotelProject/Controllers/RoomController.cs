using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class RoomController : MyBaseController
    {
        private readonly IRoomService _roomService;

        public RoomController(IRoomService roomService)
        {
            _roomService = roomService;
        }
        
        // [Authorize(Roles = "Admin, Employee")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomDto>>> GetAllRooms()
            => new JsonResult(await _roomService.GetRoomsAsync());
        
        [HttpPost("available")]
        public async Task<ActionResult<IEnumerable<RoomDto>>> GetAvailableRooms([FromBody] AvailableRoom request)
            => new JsonResult(await _roomService.GetAvailableRoomsAsync(request));
    }
}
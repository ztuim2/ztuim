using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class EmployeeController : MyBaseController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        // [Authorize(Roles = "Admin, Employee")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeDto>>> GetAllEmployees()
            => new JsonResult(await _employeeService.GetEmployeesAsync());

        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeDto>> GetEmployeeById(Guid id)
            => new JsonResult(await _employeeService.GetEmployeeByIdAsync(id));
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpPost]
        public async Task<ActionResult> CreateEmployee([FromBody] CreateEmployee request)
        {
            await _employeeService.AddEmployeeAsync(request);
            
            return CreatedAtAction(null, null, null);
        }
        
        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployee(Guid id, [FromBody] UpdateEmployee request)
        {
            request.Id = id;
            
            await _employeeService.UpdateEmployeeAsync(request);

            return NoContent();
        }
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            await _employeeService.DeleteEmployeeAsync(id);

            return NoContent();
        }
    }
}
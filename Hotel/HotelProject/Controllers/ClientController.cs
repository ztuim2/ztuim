using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class ClientController : MyBaseController
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }
        
        // [Authorize(Roles = "Admin, Employee")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientDto>>> GetAllClients()
            => new JsonResult(await _clientService.GetClientsAsync());

        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ClientDto>> GetClientById(Guid id)
            => new JsonResult(await _clientService.GetClientByIdAsync(id));
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpPost]
        public async Task<ActionResult> CreateClient([FromBody] CreateClient request)
        {
            await _clientService.AddClientAsync(request);
            
            return CreatedAtAction(null, null, null);
        }
        
        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateClient(Guid id, [FromBody] UpdateClient request)
        {
            request.Id = id;
            
            await _clientService.UpdateClientAsync(request);

            return NoContent();
        }
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteClient(Guid id)
        {
            await _clientService.DeleteClientAsync(id);

            return NoContent();
        }
    }
}
using System;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class InvoiceController : MyBaseController
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }
        
        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<InvoiceDto>> GetInvoiceAsync(Guid id)
            => new JsonResult(await _invoiceService.GetInvoiceAsync(id));
                
        //[Authorize(Roles = "Admin, Employee")]
        [HttpPost]
        public async Task<ActionResult> CreateInvoice([FromBody] CreateInvoice request)
        {
            await _invoiceService.AddInvoiceAsync(request);
                    
            return CreatedAtAction(null, null, null);
        }
    }
}
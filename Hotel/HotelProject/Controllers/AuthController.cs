using System.Threading.Tasks;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class AuthController : MyBaseController
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<string>> Login([FromBody] Login request)
            => new JsonResult(await _authService.LoginAsync(request));
    }
}
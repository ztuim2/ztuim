using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Services;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    public class ReservationController : MyBaseController
    {
        private readonly IReservationService _reservationService;
        private readonly IInvoiceService _invoiceService;

        public ReservationController(IReservationService reservationService, IInvoiceService invoiceService)
        {
            _reservationService = reservationService;
            _invoiceService = invoiceService;
        }
        
        // [Authorize(Roles = "Admin, Employee")]
        [HttpGet("client/{clientId:guid}")]
        public async Task<ActionResult<IEnumerable<ReservationDto>>> GetReservationForClient(Guid clientId)
            => new JsonResult(await _reservationService.GetReservationsForClientAsync(clientId));

        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ReservationDto>> GetReservationAsync(Guid id)
            => new JsonResult(await _reservationService.GetReservationAsync(id));
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpPost]
        public async Task<ActionResult> CreateReservation([FromBody] CreateReservation request)
        {
            await _reservationService.AddReservationAsync(request);
            
            return CreatedAtAction(null, null, null);
        }
        
        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpPut("confirm/{id:guid}")]
        public async Task<ActionResult> ConfirmReservation(Guid id)
        {
            await _reservationService.ConfirmReservationAsync(id);

            return NoContent();
        }
        
        //[Authorize(Roles = "Admin, Employee, Client")]
        [HttpPut("pay/{id:guid}")]
        public async Task<ActionResult> PayForReservation(Guid id)
        {
            await _reservationService.PayForReservationAsync(id);

            return NoContent();
        }
        
        //[Authorize(Roles = "Admin, Employee")]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteReservation(Guid id)
        {
            await _reservationService.DeleteReservationAsync(id);

            return NoContent();
        }
    }
}
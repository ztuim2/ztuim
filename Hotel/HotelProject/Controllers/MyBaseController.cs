using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace HotelProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class MyBaseController : ControllerBase
    {
        protected Guid UserId => User?.Identity?.IsAuthenticated == true ?
            Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value) : Guid.Empty;
    }
}
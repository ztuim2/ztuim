using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class RoomReservationRepository : IRoomReservationRepository
    {
        private readonly HotelContext _context;

        public RoomReservationRepository(HotelContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<Guid>> GetConflictRoomsAsync(DateTime start, DateTime end)
            => await _context.RoomReservations.Where(x => !(end < x.BookedAt || start > x.BookedTo))
                .Select(x => x.RoomId)
                .ToListAsync();
        
        public async Task AddRoomToReservationAsync(RoomReservation roomReservation)
        {
            await _context.RoomReservations.AddAsync(roomReservation);
            await _context.SaveChangesAsync();
        }
    }
}
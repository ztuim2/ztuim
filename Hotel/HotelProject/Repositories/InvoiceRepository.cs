using System;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly HotelContext _context;

        public InvoiceRepository(HotelContext context)
        {
            _context = context;
        }

        public async Task<Invoice> GetAsync(Guid id)
            => await _context.Invoices.SingleOrDefaultAsync(x => x.Id == id);

        public async Task AddInvoiceAsync(Invoice invoice)
        {
            await _context.Invoices.AddAsync(invoice);
            await _context.SaveChangesAsync();
        }
    }
}
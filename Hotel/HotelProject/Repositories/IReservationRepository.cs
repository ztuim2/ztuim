using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models;

namespace HotelProject.Repositories
{
    public interface IReservationRepository
    {
        Task<IEnumerable<Reservation>> GetReservationsForClientAsync(Guid clientId);
        Task<IEnumerable<Reservation>> GetConflictReservationsAsync(DateTime start, DateTime end);
        Task<Reservation> GetReservationAsync(Guid id);
        Task AddReservationAsync(Reservation reservation);
        Task UpdateReservationAsync(Reservation reservation);
        Task DeleteReservationAsync(Reservation reservation);
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models;

namespace HotelProject.Repositories
{
    public interface IRoomReservationRepository
    {
        Task<IEnumerable<Guid>> GetConflictRoomsAsync(DateTime start, DateTime end);
        Task AddRoomToReservationAsync(RoomReservation roomReservation);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class RoomRepository : IRoomRepository
    {
        private readonly HotelContext _context;

        public RoomRepository(HotelContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<Room>> GetRoomsAsync()
            => await _context.Rooms.ToListAsync();

        public async Task<IEnumerable<Room>> GetRoomsWithIdsAsync(IEnumerable<Guid> ids)
            => await _context.Rooms.Where(x => ids.Contains(x.Id))
                .ToListAsync();

        public async Task<Room> GetRoom(Guid id)
            => await _context.Rooms.SingleOrDefaultAsync(x => x.Id == id);
    }
}
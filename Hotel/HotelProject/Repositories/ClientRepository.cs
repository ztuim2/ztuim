﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly HotelContext _context;

        public ClientRepository(HotelContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Client>> GetClientsAsync()
            => await _context.Clients.ToListAsync();

        public async Task<Client> GetClientByIdAsync(Guid id)
            => await _context.Clients.SingleOrDefaultAsync(x => x.Id == id);

        public async Task<Client> GetClientByEmailAsync(string email)
            => await _context.Clients.SingleOrDefaultAsync(x => x.Email == email);

        public async Task AddClientAsync(Client client)
        {
            await _context.Clients.AddAsync(client);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateClientAsync(Client client)
        {
            _context.Clients.Update(client);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteClientAsync(Client client)
        {
            _context.Clients.Remove(client);
            await _context.SaveChangesAsync();
        }
    }
}
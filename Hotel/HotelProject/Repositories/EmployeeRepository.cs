﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly HotelContext _context;

        public EmployeeRepository(HotelContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync()
            => await _context.Employees.ToListAsync();

        public async Task<Employee> GetEmployeeByIdAsync(Guid id)
            => await _context.Employees.SingleOrDefaultAsync(x => x.Id == id);

        public async Task<Employee> GetEmployeeByEmailAsync(string email)
            => await _context.Employees.SingleOrDefaultAsync(x => x.Email == email);

        public async Task AddEmployeeAsync(Employee employee)
        {
            await _context.Employees.AddAsync(employee);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateEmployeeAsync(Employee employee)
        {
            _context.Employees.Update(employee);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteEmployeeAsync(Employee employee)
        {
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
        }
    }
}
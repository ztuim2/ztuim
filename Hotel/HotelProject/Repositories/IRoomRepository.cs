﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models;

namespace HotelProject.Repositories
{
    public interface IRoomRepository
    {
        Task<IEnumerable<Room>> GetRoomsAsync();
        Task<IEnumerable<Room>> GetRoomsWithIdsAsync(IEnumerable<Guid> ids);
        Task<Room> GetRoom(Guid id);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Databases;
using HotelProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelProject.Repositories
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly HotelContext _context;

        public ReservationRepository(HotelContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Reservation>> GetReservationsForClientAsync(Guid clientId)
            => await _context.Reservations.Include(x => x.Rooms)
                .Where(x => x.ClientId == clientId)
                .ToListAsync();

        public async Task<IEnumerable<Reservation>> GetConflictReservationsAsync(DateTime start, DateTime end)
            => await _context.Reservations.Include(x => x.Rooms)
                .Where(x => !(end < x.Start || start > x.End))
                .ToListAsync();

        public async Task<Reservation> GetReservationAsync(Guid id)
            => await _context.Reservations.Include(x => x.Rooms)
                .SingleOrDefaultAsync(x => x.Id == id);

        public async Task AddReservationAsync(Reservation reservation)
        {
            await _context.AddAsync(reservation);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateReservationAsync(Reservation reservation)
        {
            _context.Update(reservation);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteReservationAsync(Reservation reservation)
        {
            _context.Remove(reservation);
            await _context.SaveChangesAsync();
        }
    }
}
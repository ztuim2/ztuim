using System;
using System.Threading.Tasks;
using HotelProject.Models;

namespace HotelProject.Repositories
{
    public interface IInvoiceRepository
    {
        Task<Invoice> GetAsync(Guid id);
        Task AddInvoiceAsync(Invoice invoice);
    }
}
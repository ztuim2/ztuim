﻿using System;
using System.Collections.Generic;
using HotelProject.Models.Enums;

namespace HotelProject.Models
{
    public class Room
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }

        public ICollection<RoomReservation> Reservations { get; set; }

        public Room()
        {
            Reservations = new List<RoomReservation>();
        }
    }
}
using System;

namespace HotelProject.Models.RequestModels
{
    public class CreateInvoice
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public Guid EmployeeId { get; set; }
        public Guid ReservationId { get; set; }
        public DateTime Issue { get; set; }

        public CreateInvoice() { }

        public CreateInvoice(Guid id, Guid clientId, Guid employeeId, Guid reservationId, DateTime issue)
        {
            Id = id == Guid.Empty ? Guid.NewGuid() : id;
            ClientId = clientId;
            EmployeeId = employeeId;
            ReservationId = reservationId;
            Issue = issue;
        }  
    }
}
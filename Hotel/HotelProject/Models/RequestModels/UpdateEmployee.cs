using System;

namespace HotelProject.Models.RequestModels
{
    public class UpdateEmployee
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public UpdateEmployee() { }

        public UpdateEmployee(Guid id,string firstName, string lastName, string position, string email,
            string password, string role)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Position = position;
            Email = email;
            Password = password;
            Role = role;
        }
    }
}
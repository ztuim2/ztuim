using System;
using System.Collections.Generic;

namespace HotelProject.Models.RequestModels
{
    public class CreateReservation
    {
        public Guid? Id { get; set; }
        public string Description { get; set; }
        public Guid? ClientId { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string Remarks { get; set; }
        public IEnumerable<Guid> RoomIds { get; set; }

        public CreateReservation() { }

        public CreateReservation(Guid id, string description, Guid? clientId, string start, string end,
            string remarks, IEnumerable<Guid> roomIds)
        {
            Id = id == Guid.Empty ? Guid.NewGuid() : id;
            Description = description;
            ClientId = clientId;
            Start = start;
            End = end;
            Remarks = remarks;
            RoomIds = roomIds;
        }
    }
}
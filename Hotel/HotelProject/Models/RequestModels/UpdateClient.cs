using System;

namespace HotelProject.Models.RequestModels
{
    public class UpdateClient
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public UpdateClient() { }

        public UpdateClient(Guid id, string firstName, string lastName, string phoneNumber, string email, string password)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Email = email;
            Password = password;
        }
    }
}
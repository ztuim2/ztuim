using System;

namespace HotelProject.Models.RequestModels
{
    public class AvailableRoom
    {
        public string Start { get; set; }
        public string End { get; set; }

        public AvailableRoom() { }

        public AvailableRoom(string start, string end)
        {
            Start = start;
            End = end;
        }
    }
}
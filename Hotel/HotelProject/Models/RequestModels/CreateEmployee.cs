using System;

namespace HotelProject.Models.RequestModels
{
    public class CreateEmployee
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public CreateEmployee() { }

        public CreateEmployee(Guid id,string firstName, string lastName, string position, string email,
            string password, string role)
        {
            Id = id == Guid.Empty ? Guid.NewGuid() : id;
            FirstName = firstName;
            LastName = lastName;
            Position = position;
            Email = email;
            Password = password;
            Role = role;
        }
    }
}
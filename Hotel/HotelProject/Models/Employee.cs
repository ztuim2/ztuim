﻿namespace HotelProject.Models
{
    public class Employee : User
    {
        public string Position { get; set; }

        public Invoice Invoice { get; set; }
    }
}
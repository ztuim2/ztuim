﻿using System;

namespace HotelProject.Models
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public string ClientName { get; set; }
        public Guid EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public Guid ReservationId { get; set; }
        public DateTime Issue { get; set; }
        public DateTime Pay { get; set; }
        public decimal ToPay { get; set; }

        public Employee Employee { get; set; }
        public Client Client { get; set; }
        public Reservation Reservation { get; set; }
    }
}
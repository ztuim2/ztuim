﻿using System;
using System.Collections.Generic;
using HotelProject.Models.Enums;

namespace HotelProject.Models
{
    public class Reservation
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }
        public decimal Price { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public ReservationStatus ReservationStatus { get; set; }
        public PayStatus PayStatus { get; set; }
        public string Remarks { get; set; }

        public Invoice Invoice { get; set; }
        public Client Client { get; set; }
        public ICollection<RoomReservation> Rooms { get; set; }

        public Reservation()
        {
            Rooms = new List<RoomReservation>();
        }
    }
}
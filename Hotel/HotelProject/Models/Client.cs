﻿using System;
using System.Collections.Generic;

namespace HotelProject.Models
{
    public class Client : User
    {
        public string PhoneNumber { get; set; }

        public Invoice Invoice { get; set; }
        public ICollection<Reservation> Reservations { get; set; }

        public Client()
        {
            Reservations = new List<Reservation>();
        }
    }
}
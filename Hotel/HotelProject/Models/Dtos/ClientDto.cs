using System;

namespace HotelProject.Models.Dtos
{
    public class ClientDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
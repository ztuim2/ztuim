using System;
using System.Collections;
using System.Collections.Generic;

namespace HotelProject.Models.Dtos
{
    public class ReservationDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public Guid? ClientId { get; set; }
        public string ClientName { get; set; }
        public decimal Price { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string ReservationStatus { get; set; }
        public string PayStatus { get; set; }
        public string Remarks { get; set; }
        public IEnumerable<RoomDto> Rooms { get; set; }
    }
}
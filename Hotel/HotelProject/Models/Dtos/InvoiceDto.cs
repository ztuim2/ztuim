using System;

namespace HotelProject.Models.Dtos
{
    public class InvoiceDto
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public string Client { get; set; }
        public Guid EmployeeId { get; set; }
        public string Employee { get; set; }
        public Guid ReservationId { get; set; }
        public DateTime Issue { get; set; }
        public DateTime Pay { get; set; }
        public decimal ToPay { get; set; }
    }
}
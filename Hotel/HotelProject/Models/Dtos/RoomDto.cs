using System;

namespace HotelProject.Models.Dtos
{
    public class RoomDto
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public decimal Price { get; set; }
        public string Type { get; set; }
    }
}
using System;

namespace HotelProject.Models.Dtos
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Position { get; set; }
    }
}
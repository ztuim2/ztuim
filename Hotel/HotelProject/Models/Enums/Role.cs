﻿namespace HotelProject.Models.Enums
{
    public enum Role
    {
        Admin = 0,
        Employee,
        Client
    }
}
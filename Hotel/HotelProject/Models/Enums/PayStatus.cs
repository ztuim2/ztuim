﻿namespace HotelProject.Models.Enums
{
    public enum PayStatus
    {
        Paid = 0,
        NoPaid
    }
}
﻿namespace HotelProject.Models.Enums
{
    public enum ReservationStatus
    {
        Confirmed = 0,
        Unconfirmed
    }
}
﻿namespace HotelProject.Models.Enums
{
    public enum RoomType
    {
        Sgl = 0,
        Dbl,
        Trl,
        Fpr
    }
}
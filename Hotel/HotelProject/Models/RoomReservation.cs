﻿using System;

namespace HotelProject.Models
{
    public class RoomReservation
    {
        public Guid ReservationId { get; set; }
        public Guid RoomId { get; set; }
        public DateTime BookedAt { get; set; }
        public DateTime BookedTo { get; set; }
        public decimal Value { get; set; }

        public Reservation Reservation { get; set; }
        public Room Room { get; set; }
    }
}
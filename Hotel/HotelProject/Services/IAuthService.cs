using System.Threading.Tasks;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IAuthService
    {
        Task<string> LoginAsync(Login request);
    }
}
using System;
using System.Threading.Tasks;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;
using HotelProject.Utils;

namespace HotelProject.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IReservationRepository _reservationRepository;
        private readonly IInvoiceRepository _invoiceRepository;

        public InvoiceService(IClientRepository clientRepository, IEmployeeRepository employeeRepository, 
            IReservationRepository reservationRepository, IInvoiceRepository invoiceRepository)
        {
            _clientRepository = clientRepository;
            _employeeRepository = employeeRepository;
            _reservationRepository = reservationRepository;
            _invoiceRepository = invoiceRepository;
        }
        
        public async Task<InvoiceDto> GetInvoiceAsync(Guid id)
        {
            var invoice = await _invoiceRepository.GetAsync(id);

            return Mapper.InvoiceToDto(invoice);
        }

        public async Task AddInvoiceAsync(CreateInvoice request)
        {
            var client = await _clientRepository.GetClientByIdAsync(request.ClientId);
            var employee = await _employeeRepository.GetEmployeeByIdAsync(request.EmployeeId);
            var reservation = await _reservationRepository.GetReservationAsync(request.ReservationId);

            var invoice = new Invoice
            {
                Id = request.Id,
                ClientId = request.ClientId,
                ClientName = $"{client.FirstName} {client.LastName}",
                EmployeeId = request.EmployeeId,
                EmployeeName = $"{employee.FirstName} {employee.LastName}",
                ReservationId = request.ReservationId,
                Issue = request.Issue,
                Pay = request.Issue,
                ToPay = reservation.Price
            };

            await _reservationRepository.AddReservationAsync(reservation);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;
using HotelProject.Utils;

namespace HotelProject.Services
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IRoomReservationRepository _roomReservationRepository;

        public RoomService(IRoomRepository roomRepository, IRoomReservationRepository roomReservationRepository)
        {
            _roomRepository = roomRepository;
            _roomReservationRepository = roomReservationRepository;
        }
        
        public async Task<IEnumerable<RoomDto>> GetRoomsAsync()
        {
            var rooms = await _roomRepository.GetRoomsAsync();

            return rooms.Select(Mapper.RoomToDto);
        }

        public async Task<IEnumerable<RoomDto>> GetAvailableRoomsAsync(AvailableRoom request)
        {
            var rooms = await _roomRepository.GetRoomsAsync();
            var conflictIds = await _roomReservationRepository.GetConflictRoomsAsync(DateTime.Parse(request.Start), DateTime.Parse(request.End));
            var removedDuplicateIds = conflictIds.ToHashSet();
            
            var returnRooms = rooms.Where(x => !removedDuplicateIds.Contains(x.Id))
                .ToList();
            

            return returnRooms.Select(Mapper.RoomToDto);
        }
    }
}
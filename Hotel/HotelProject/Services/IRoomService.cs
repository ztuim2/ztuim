using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IRoomService
    {
        Task<IEnumerable<RoomDto>> GetRoomsAsync();
        Task<IEnumerable<RoomDto>> GetAvailableRoomsAsync(AvailableRoom request);
    }
}
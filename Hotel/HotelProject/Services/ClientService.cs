using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;
using HotelProject.Utils;
using System.Linq;
using HotelProject.Auths;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.Enums;

namespace HotelProject.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IPasswordHasher _passwordHasher;

        public ClientService(IClientRepository clientRepository, IPasswordHasher passwordHasher)
        {
            _clientRepository = clientRepository;
            _passwordHasher = passwordHasher;
        }

        public async Task<IEnumerable<ClientDto>> GetClientsAsync()
        {
            var clients = await _clientRepository.GetClientsAsync();

            return clients.Select(Mapper.ClientToDto);
        }

        public async Task<ClientDto> GetClientByIdAsync(Guid id)
        {
            var client = await _clientRepository.GetClientByIdAsync(id);

            if (client == null)
            {
                throw new Exception($"Client with id: {id} doesn't exist.");
            }

            return Mapper.ClientToDto(client);
        }

        public async Task AddClientAsync(CreateClient request)
        {
            var client = await _clientRepository.GetClientByEmailAsync(request.Email);

            if (client != null)
            {
                throw new Exception($"Client with email: {request.Email} exists.");
            }

            client = new Client
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                Password = _passwordHasher.Hash(request.Password),
                Role = Role.Client
            };

            await _clientRepository.AddClientAsync(client);
        }

        public async Task UpdateClientAsync(UpdateClient request)
        {
            var client = await _clientRepository.GetClientByIdAsync(request.Id);

            if (client == null)
            {
                throw new Exception($"Client with id: {request.Id} doesn't exist.");
            }

            if (client.Email != request.Email)
            {
                client.Email = request.Email;
            }

            if (!_passwordHasher.Check(client.Password, request.Password) && !string.IsNullOrWhiteSpace(request.Password))
            {
                client.Password = _passwordHasher.Hash(request.Password);
            }
            
            if (client.FirstName != request.FirstName)
            {
                client.FirstName = request.FirstName;
            }
            
            if (client.LastName != request.LastName)
            {
                client.LastName = request.LastName;
            }
            
            if (client.PhoneNumber != request.PhoneNumber)
            {
                client.PhoneNumber = request.PhoneNumber;
            }

            await _clientRepository.UpdateClientAsync(client);
        }

        public async Task DeleteClientAsync(Guid id)
        {
            var client = await _clientRepository.GetClientByIdAsync(id);

            if (client == null)
            {
                throw new Exception($"Client with id: {id} doesn't exist.");
            }

            await _clientRepository.DeleteClientAsync(client);
        }
    }
}
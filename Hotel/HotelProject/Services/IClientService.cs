using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IClientService
    {
        Task<IEnumerable<ClientDto>> GetClientsAsync();
        Task<ClientDto> GetClientByIdAsync(Guid id);
        Task AddClientAsync(CreateClient request);
        Task UpdateClientAsync(UpdateClient request);
        Task DeleteClientAsync(Guid id);
    }
}
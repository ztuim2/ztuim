using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Auths;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.Enums;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;
using HotelProject.Utils;

namespace HotelProject.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPasswordHasher _passwordHasher;

        public EmployeeService(IEmployeeRepository employeeRepository, IPasswordHasher passwordHasher)
        {
            _employeeRepository = employeeRepository;
            _passwordHasher = passwordHasher;
        }
        
        public async Task<IEnumerable<EmployeeDto>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetEmployeesAsync();

            return employees.Select(Mapper.EmployeeToDto);
        }

        public async Task<EmployeeDto> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetEmployeeByIdAsync(id);

            if (employee == null)
            {
                throw new Exception($"Employee with id: {id} doesn't exist.");
            }

            return Mapper.EmployeeToDto(employee);
        }

        public async Task AddEmployeeAsync(CreateEmployee request)
        {
            var employee = await _employeeRepository.GetEmployeeByEmailAsync(request.Email);

            if (employee != null)
            {
                throw new Exception($"Employee with email: {request.Email} exists.");
            }

            employee = new Employee
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Position = request.Position,
                Password = _passwordHasher.Hash(request.Password),
                Role = Enum.Parse<Role>(request.Role)
            };

            await _employeeRepository.AddEmployeeAsync(employee);
        }

        public async Task UpdateEmployeeAsync(UpdateEmployee request)
        {
            var employee = await _employeeRepository.GetEmployeeByIdAsync(request.Id);

            if (employee == null)
            {
                throw new Exception($"Employee with id: {request.Id} doesn't exist.");
            }

            if (employee.Email != request.Email)
            {
                employee.Email = request.Email;
            }

            if (!_passwordHasher.Check(employee.Password, request.Password) &&
                !string.IsNullOrWhiteSpace(request.Password))
            {
                employee.Password = _passwordHasher.Hash(request.Password);
            }

            if (employee.FirstName != request.FirstName)
            {
                employee.FirstName = request.FirstName;
            }

            if (employee.LastName != request.LastName)
            {
                employee.LastName = request.LastName;
            }

            if (employee.Position != request.Position && request.Position != null)
            {
                employee.Position = request.Position;
            }

            if (employee.Role.ToString() != request.Role && request.Position != null)
            {
                employee.Role = Enum.Parse<Role>(request.Role);
            }

            await _employeeRepository.UpdateEmployeeAsync(employee);
        }

        public async Task DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetEmployeeByIdAsync(id);

            if (employee == null)
            {
                throw new Exception($"Employee with id: {id} doesn't exist.");
            }

            await _employeeRepository.DeleteEmployeeAsync(employee);
        }
    }
}
using System;
using System.Threading.Tasks;
using HotelProject.Auths;
using HotelProject.Models;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;

namespace HotelProject.Services
{
    public class AuthService : IAuthService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IJwtHandler _jwtHandler;
        private readonly IPasswordHasher _passwordHasher;

        public AuthService(IClientRepository clientRepository, IEmployeeRepository employeeRepository,
            IJwtHandler jwtHandler, IPasswordHasher passwordHasher)
        {
            _clientRepository = clientRepository;
            _employeeRepository = employeeRepository;
            _jwtHandler = jwtHandler;
            _passwordHasher = passwordHasher;
        }
        
        public async Task<string> LoginAsync(Login request)
        {
            var client = await _clientRepository.GetClientByEmailAsync(request.Email);

            if (IsUserValid(client, request.Password))
            {
                return CreateToken(client, _jwtHandler);
            }

            var employee = await _employeeRepository.GetEmployeeByEmailAsync(request.Email);

            if (IsUserValid(employee, request.Password))
            {
                return CreateToken(employee, _jwtHandler);
            }
            
            throw new Exception("Invalid credentials.");
        }

        private bool IsUserValid<T>(T user, string password) where T : User
        {
            return user != null && _passwordHasher.Check(user.Password, password);
        }

        private string CreateToken<T>(T user, IJwtHandler jwtHandler) where T : User
            => jwtHandler.CreateToken(user.Id, $"{ user.FirstName } { user.LastName }", user.Role.ToString());
    }
}
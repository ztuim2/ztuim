using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IEmployeeService
    {
        Task<IEnumerable<EmployeeDto>> GetEmployeesAsync();
        Task<EmployeeDto> GetEmployeeByIdAsync(Guid id);
        Task AddEmployeeAsync(CreateEmployee request);
        Task UpdateEmployeeAsync(UpdateEmployee request);
        Task DeleteEmployeeAsync(Guid id);
    }
}
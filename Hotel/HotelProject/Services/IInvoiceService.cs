using System;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IInvoiceService
    {
        Task<InvoiceDto> GetInvoiceAsync(Guid id);
        Task AddInvoiceAsync(CreateInvoice request);
    }
}
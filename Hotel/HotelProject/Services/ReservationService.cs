using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelProject.Models;
using HotelProject.Models.Dtos;
using HotelProject.Models.Enums;
using HotelProject.Models.RequestModels;
using HotelProject.Repositories;
using HotelProject.Utils;

namespace HotelProject.Services
{
    public class ReservationService : IReservationService
    {
        private readonly IReservationRepository _reservationRepository;
        private readonly IRoomReservationRepository _roomReservationRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IClientRepository _clientRepository;

        public ReservationService(IReservationRepository reservationRepository, 
            IRoomReservationRepository roomReservationRepository, IRoomRepository roomRepository, IClientRepository clientRepository)
        {
            _reservationRepository = reservationRepository;
            _roomReservationRepository = roomReservationRepository;
            _roomRepository = roomRepository;
            _clientRepository = clientRepository;
        }
        
        public async Task<IEnumerable<ReservationDto>> GetReservationsForClientAsync(Guid clientId)
        {
            var reservations = await _reservationRepository.GetReservationsForClientAsync(clientId);

            return reservations.Select(x => Mapper.ReservationToDto(x, _roomRepository));
        }

        public async Task<ReservationDto> GetReservationAsync(Guid id)
        {
            var reservation = await _reservationRepository.GetReservationAsync(id);

            if (reservation == null)
            {
                throw new Exception($"Reservation with id: {id} doesn't exist.");
            }

            return Mapper.ReservationToDto(reservation, _roomRepository);
        }

        public async Task AddReservationAsync(CreateReservation request)
        {
            var reservation = new Reservation();
            var client = await _clientRepository.GetClientByIdAsync((Guid)request.ClientId);
            var rooms = await _roomRepository.GetRoomsWithIdsAsync(request.RoomIds);
            
            var conflictReservations = await _reservationRepository.GetConflictReservationsAsync(DateTime.Parse(request.Start), DateTime.Parse(request.End));

            if (conflictReservations == null)
            {
                reservation.Id = (Guid) request.Id;
                reservation.Description = request.Description;
                reservation.ClientId = request.ClientId;
                reservation.ClientName = $"{client.FirstName} {client.LastName}";
                reservation.Start = DateTime.Parse(request.Start);
                reservation.End = DateTime.Parse(request.End);
                reservation.ReservationStatus = ReservationStatus.Unconfirmed;
                reservation.PayStatus = PayStatus.NoPaid;
                reservation.Remarks = request.Remarks;
                reservation.Price = rooms.Sum(room => room.Price) * (DateTime.Parse(request.End) - DateTime.Parse(request.Start)).Days;

                await _reservationRepository.AddReservationAsync(reservation);

                foreach (var room in rooms)
                {
                    await _roomReservationRepository.AddRoomToReservationAsync(new RoomReservation
                    {
                        BookedAt = DateTime.Parse(request.Start),
                        BookedTo = DateTime.Parse(request.End),
                        ReservationId = reservation.Id,
                        RoomId = room.Id,
                        Value = room.Price
                    });
                }
            }

            else
            {
                foreach (var conflictReservation in conflictReservations)
                {
                    foreach (var reservationRoom in conflictReservation.Rooms)
                    {
                        foreach (var room in rooms)
                        {
                            if (reservationRoom.RoomId == room.Id)
                            {
                                throw new Exception($"Room with id {room.Id} can't be add to reservation.");
                            }
                        }
                    }
                }
                
                reservation.Id = (Guid) request.Id;
                reservation.Description = request.Description;
                reservation.ClientId = request.ClientId;
                reservation.ClientName = $"{client.FirstName} {client.LastName}";
                reservation.Start = DateTime.Parse(request.Start);
                reservation.End = DateTime.Parse(request.End);
                reservation.ReservationStatus = ReservationStatus.Unconfirmed;
                reservation.PayStatus = PayStatus.NoPaid;
                reservation.Remarks = request.Remarks;
                reservation.Price = rooms.Sum(room => room.Price) * (DateTime.Parse(request.End) - DateTime.Parse(request.Start)).Days;
                
                await _reservationRepository.AddReservationAsync(reservation);
                
                foreach (var room in rooms)
                {
                    await _roomReservationRepository.AddRoomToReservationAsync(new RoomReservation
                    {
                        BookedAt = DateTime.Parse(request.Start),
                        BookedTo = DateTime.Parse(request.End),
                        ReservationId = reservation.Id,
                        RoomId = room.Id,
                        Value = room.Price
                    });
                }
            }
        }

        public async Task ConfirmReservationAsync(Guid id)
        {
            var reservation = await _reservationRepository.GetReservationAsync(id);
            
            if (reservation == null)
            {
                throw new Exception($"Reservation with id: {id} doesn't exist.");
            }

            reservation.ReservationStatus = ReservationStatus.Confirmed;

            await _reservationRepository.UpdateReservationAsync(reservation);
        }

        public async Task PayForReservationAsync(Guid id)
        {
            var reservation = await _reservationRepository.GetReservationAsync(id);
            
            if (reservation == null)
            {
                throw new Exception($"Reservation with id: {id} doesn't exist.");
            }

            reservation.PayStatus = PayStatus.Paid;

            await _reservationRepository.UpdateReservationAsync(reservation);
        }

        public async Task DeleteReservationAsync(Guid id)
        {
            var reservation = await _reservationRepository.GetReservationAsync(id);

            if (reservation == null)
            {
                throw new Exception($"Reservation with id: {id} doesn't exist.");
            }

            await _reservationRepository.DeleteReservationAsync(reservation);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelProject.Models.Dtos;
using HotelProject.Models.RequestModels;

namespace HotelProject.Services
{
    public interface IReservationService
    {
        Task<IEnumerable<ReservationDto>> GetReservationsForClientAsync(Guid clientId);
        Task<ReservationDto> GetReservationAsync(Guid id);
        Task AddReservationAsync(CreateReservation request);
        Task ConfirmReservationAsync(Guid id);
        Task PayForReservationAsync(Guid id);
        Task DeleteReservationAsync(Guid id);
    }
}
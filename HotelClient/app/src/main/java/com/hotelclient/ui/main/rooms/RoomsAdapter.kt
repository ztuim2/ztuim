package com.hotelclient.ui.main.rooms

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.internal.ContextUtils.getActivity
import com.hotelclient.MyApplication
import com.hotelclient.R
import com.hotelclient.api.ReservationInterface
import com.hotelclient.models.Reservation
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.reservation.ReservationFragment
import com.hotelclient.utillities.TokenExplorator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.Console
import java.net.HttpURLConnection
import javax.inject.Inject

class RoomsAdapter(private val roomsList:List<Room>, private val searchDate: SearchDate, private val activity: FragmentActivity) : RecyclerView.Adapter<RoomsViewHolder>() {
    var retrofit: Retrofit? = null @Inject set
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomsViewHolder {
        MyApplication.appComponent?.inject(this)
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.room_card_view, parent, false)
        return RoomsViewHolder(view)
    }

    override fun onBindViewHolder(holder: RoomsViewHolder, position: Int) {
        if (roomsList != null) {
            holder.roomName?.setText(roomsList.get(position).type)
            val price = roomsList.get(position).price.toString() + " zł/doba"
            holder.roomPrice?.setText(price)
            holder.reservationButton?.setOnClickListener { e -> reserveRoom(position,holder) }
        }
    }

    private fun reserveRoom(position: Int,holder: RoomsViewHolder) {
        val reservationService: ReservationInterface = retrofit!!.create(ReservationInterface::class.java)
        val rooms = listOf<String>(roomsList.get(position).id)
        val reserv = Reservation(
            "00000000-0000-0000-0000-000000000000","", TokenExplorator.getUserId(holder.reservationButton!!.context)!!,
            searchDate.start,searchDate.end,"",rooms)
        val call: Call<Void> = reservationService.reserve(reserv)
        call?.enqueue(object : Callback<Void>{
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    val fragmentTransaction: FragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.main_nav_host_fragment, ReservationFragment()).addToBackStack(null)
                    fragmentTransaction.commit()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    override fun getItemCount(): Int {
        return if (roomsList != null) roomsList.size else 0
    }

}
package com.hotelclient.ui.main.rooms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.hotelclient.R
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.search.SearchViewModel
import java.util.*

class RoomsFragment : Fragment() {
    private lateinit var searchViewModel: SearchViewModel
    private var rooms: List<Room> = ArrayList<Room>()
    private var mRecyclerView: RecyclerView? = null
    private var roomsAdapter: RoomsAdapter? = null
    private var roomsDate: SearchDate? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_rooms, container, false)
        searchViewModel = activity?.run {
            ViewModelProviders.of(this).get(SearchViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        searchViewModel.searchDate.observe(viewLifecycleOwner, Observer {
            roomsDate = searchViewModel.searchDate.value;
            val mViewModel: RoomsViewModel by viewModels { RoomsViewModelFactory(this,roomsDate) }
            mRecyclerView = view.findViewById(R.id.roomsRecyclerView)
            roomsAdapter = RoomsAdapter(rooms, roomsDate!!, activity!!)
            mRecyclerView?.setAdapter(roomsAdapter)

            mViewModel.getProductViews()?.observe(viewLifecycleOwner) { roomViews ->
                rooms = roomViews
                roomsAdapter = RoomsAdapter(rooms, roomsDate!!,activity!!)
                mRecyclerView?.setAdapter(roomsAdapter)
            }
        })



        return view
    }
}
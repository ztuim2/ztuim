package com.hotelclient.ui.auth

import UserInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.hotelclient.MyApplication
import com.hotelclient.R
import com.hotelclient.models.UserRegistration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.internal.EverythingIsNonNull
import java.net.HttpURLConnection
import javax.inject.Inject


class RegisterFragment : Fragment() {
    var retrofit: Retrofit? = null @Inject set
    private var firstNameInput: EditText? = null
    private var lastNameInput: EditText? = null
    private var phoneInput: EditText? = null
    private var emailInput: EditText? = null
    private var passwordInput: EditText? = null
    private var repasswordInput: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_register, container, false)
        MyApplication.appComponent?.inject(this)
        assignItems(view)
        return view
    }

    private fun assignItems(view: View) {
        emailInput = view.findViewById(R.id.new_user_email)
        passwordInput = view.findViewById(R.id.new_user_password)
        repasswordInput = view.findViewById(R.id.new_user_repassword)
        firstNameInput = view.findViewById(R.id.firstName)
        lastNameInput = view.findViewById(R.id.lastName)
        phoneInput = view.findViewById(R.id.phone)
        val backToLoginButton = view.findViewById<Button>(R.id.back_to_login_button)
        val signUpButton = view.findViewById<Button>(R.id.sign_up_button)
        backToLoginButton.setOnClickListener { view: View ->
            backToLoginFragment(
                view
            )
        }
        signUpButton.setOnClickListener { view: View ->
            signUp(
                view
            )
        }
    }

    private fun backToLoginFragment(view: View) {
        val action = RegisterFragmentDirections.actionNavRegisterToNavLogin()
        Navigation.findNavController(view).navigate(action)
    }

    private fun signUp(view: View) {
        if (!checkIfPasswordsMatches()) {
            Toast.makeText(
                context,
                getString(R.string.not_matching_passwords_toast),
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        val registrationUser: UserRegistration = createUserWithRegistrationCredentials()
        registerNewUser(view, registrationUser)
    }

    private fun checkIfPasswordsMatches(): Boolean {
        return passwordInput!!.text.toString() == repasswordInput!!.text.toString()
    }

    private fun createUserWithRegistrationCredentials(): UserRegistration {
        return UserRegistration(firstNameInput!!.text.toString(),lastNameInput!!.text.toString(),phoneInput!!.text.toString(), emailInput!!.text.toString(), passwordInput!!.text.toString())
    }

    private fun registerNewUser(view: View, registrationUser: UserRegistration) {
        val userService: UserInterface = retrofit!!.create(UserInterface::class.java)
        val call: Call<Void> = userService.signUp(registrationUser)
        call?.enqueue(object : Callback<Void?> {
            @EverythingIsNonNull
            override fun onResponse(call: Call<Void?>, response: Response<Void?>) {
                if (response.code() == HttpURLConnection.HTTP_CREATED) {
                    backToLoginFragment(view)
                    Toast.makeText(
                        context,
                        getString(R.string.successful_registration_toast),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            @EverythingIsNonNull
            override fun onFailure(call: Call<Void?>, t: Throwable) {
                Toast.makeText(
                    context,
                    getString(R.string.failed_retrofit_request_toast),
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}
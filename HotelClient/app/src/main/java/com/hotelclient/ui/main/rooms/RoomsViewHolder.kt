package com.hotelclient.ui.main.rooms

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hotelclient.R

class RoomsViewHolder(view: View): RecyclerView.ViewHolder(view) {
    var roomName: TextView? = null
    var roomPrice: TextView? = null
    var reservationButton: Button? = null

    init {
        roomName = itemView.findViewById(R.id.roomName)
        roomPrice = itemView.findViewById(R.id.price)
        reservationButton = itemView.findViewById(R.id.reservationButton)
    }
}
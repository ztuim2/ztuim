package com.hotelclient.ui.main.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hotelclient.models.SearchDate

class SearchViewModel : ViewModel() {
    val searchDate = MutableLiveData<SearchDate>()

    fun searchDate(item: SearchDate){
        searchDate.value = item
    }
}
package com.hotelclient.ui.auth

import UserInterface
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.hotelclient.MyApplication
import com.hotelclient.R
import com.hotelclient.models.User
import com.hotelclient.ui.main.MainActivity
import com.hotelclient.utillities.ActivityChanger
import com.hotelclient.utillities.SharedPreferencesManager
import com.hotelclient.utillities.TokenExplorator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.internal.EverythingIsNonNull
import java.net.HttpURLConnection
import java.util.*
import javax.inject.Inject

class LoginFragment : Fragment() {
    var retrofit: Retrofit? = null @Inject set
    private var emailInput: EditText? = null
    private var passwordInput: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        MyApplication.appComponent?.inject(this)
        assignItems(view)
        return view
    }

    private fun assignItems(view: View) {
        emailInput = view.findViewById(R.id.email)
        passwordInput = view.findViewById(R.id.password)
        val signInButton = view.findViewById<Button>(R.id.sign_in_button)
        val signUp = view.findViewById<TextView>(R.id.nav_register)
        setButtonsListeners(signInButton, signUp)
    }

    private fun setButtonsListeners(signInButton: Button, signUp: TextView) {
        signInButton.setOnClickListener { e: View? -> signIn() }
        signUp.setOnClickListener { view: View ->
            signUp(
                view
            )
        }
    }

    private fun signIn() {
        val user: User = createUserWithLoginCredentials()
        val userService: UserInterface = retrofit!!.create(UserInterface::class.java)
        val call: Call<String> = userService.signIn(user)
        call?.enqueue(object : Callback<String> {
            @EverythingIsNonNull
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    successfulAuthentication(response)
                                        ActivityChanger.change(
                                            Objects.requireNonNull(context)!!,
                                            MainActivity::class.java
                   )
                } else {
                    failedAuthentication()
                }
            }

            @EverythingIsNonNull
            override fun onFailure(call: Call<String>, t: Throwable) {
                Toast.makeText(
                    context,
                    getString(R.string.failed_retrofit_request_toast),
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun createUserWithLoginCredentials(): User {
        return User(emailInput!!.text.toString(), passwordInput!!.text.toString())
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun successfulAuthentication(response: Response<*>) {
        val authorization = response.body().toString()
        SharedPreferencesManager.saveToken(Objects.requireNonNull(context)!!, authorization)
        val userName = TokenExplorator.getUserNameFromToken(context!!)
        showWelcomeMessage(userName)
    }

    private fun failedAuthentication() {
        Toast.makeText(
            context,
            getString(R.string.wrong_email_or_password_toast),
            Toast.LENGTH_SHORT
        ).show()
        passwordInput!!.setText("")
    }

    private fun signUp(view: View) {
        val action: NavDirections = LoginFragmentDirections.actionNavLoginToNavRegister()
        Navigation.findNavController(view).navigate(action)
    }

    private fun showWelcomeMessage(userName: String) {
        Toast.makeText(
            context,
            String.format(getString(R.string.welcome_toast), userName),
            Toast.LENGTH_SHORT
        ).show()
    }
}
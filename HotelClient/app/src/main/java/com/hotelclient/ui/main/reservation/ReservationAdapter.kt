package com.hotelclient.ui.main.reservation

import android.app.AlertDialog
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.hotelclient.MyApplication
import com.hotelclient.R
import com.hotelclient.api.ReservationInterface
import com.hotelclient.models.ReservationView
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.order.Amount
import com.paypal.checkout.order.AppContext
import com.paypal.checkout.order.Order
import com.paypal.checkout.order.PurchaseUnit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject


class ReservationAdapter(private val reservationList:List<ReservationView>, private val activity: FragmentActivity) : RecyclerView.Adapter<ReservationViewHolder>() {

    var retrofit: Retrofit? = null @Inject set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReservationViewHolder {
        MyApplication.appComponent?.inject(this)
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.reservation_card_view, parent, false)

        return ReservationViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReservationViewHolder, position: Int) {
        if (reservationList != null) {
            holder.roomName?.setText(reservationList.get(position).rooms[0].type)
            val price = reservationList.get(position).price.toString() + " zł"
            holder.roomPrice?.setText(price)
            holder.dateFrom?.setText(reservationList.get(position).start)
            holder.dateTo?.setText(reservationList.get(position).end)
            if(reservationList.get(position).payStatus=="NoPaid") {
                holder.payStatus?.setText("Nie opłacono")
                holder.payButton?.setup(
                    createOrder =
                    CreateOrder { createOrderActions ->
                        val order =
                            Order(
                                intent = OrderIntent.CAPTURE,
                                appContext = AppContext(userAction = UserAction.PAY_NOW),
                                purchaseUnitList =
                                listOf(
                                    PurchaseUnit(
                                        amount =
                                        Amount(currencyCode = CurrencyCode.PLN, value = reservationList.get(position).price.toString())
                                    )
                                )
                            )
                        createOrderActions.create(order)
                    },
                    onApprove =
                    OnApprove { approval ->
                        approval.orderActions.capture { captureOrderResult ->
                            payReservation(position)
                        }
                    }
                )
            } else {
                holder.payStatus?.setText("Opłacono")
                holder.payButton?.setEnabled(false)
            }

            holder.deleteButton?.setOnClickListener { e -> deleteReservation(position) }

        }
    }

    private fun deleteReservation(position: Int) {
        val reservationService: ReservationInterface = retrofit!!.create(ReservationInterface::class.java)
        val call: Call<Void> = reservationService.deleteReservation(reservationList.get(position).id)
        call?.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                val fragmentTransaction: FragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.main_nav_host_fragment, ReservationFragment()).addToBackStack(null)
                fragmentTransaction.commit()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    private fun payReservation(position: Int) {
        val reservationService: ReservationInterface = retrofit!!.create(ReservationInterface::class.java)
        val call: Call<Void> = reservationService.payReservation(reservationList.get(position).id)
        call?.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                val fragmentTransaction: FragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.main_nav_host_fragment, ReservationFragment()).addToBackStack(null)
                fragmentTransaction.commit()
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    override fun getItemCount(): Int {
        return if (reservationList != null) reservationList.size else 0
    }
}
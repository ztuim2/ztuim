package com.hotelclient.ui.main.rooms

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.hotelclient.MyApplication
import com.hotelclient.api.RoomInterface
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.search.SearchViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.net.HttpURLConnection
import javax.inject.Inject

class RoomsViewModel(private val mApplication: RoomsFragment, searchDate: SearchDate?) : ViewModel() {
    var retrofit: Retrofit? = null @Inject set
    var roomsView = MutableLiveData<List<Room>>()

    init {
        MyApplication.appComponent?.inject(this)
        getRooms(searchDate);
    }

    fun getProductViews(): MutableLiveData<List<Room>>? {
        return roomsView
    }

     fun getRooms(searchDate: SearchDate?): MutableLiveData<List<Room>>? {

        val roomService: RoomInterface = retrofit!!.create(RoomInterface::class.java)
        val call: Call<List<Room>> = roomService.getAvailableRooms(searchDate)
        call?.enqueue(object : Callback<List<Room>> {
            override fun onResponse(call: Call<List<Room>>, response: Response<List<Room>>) {
                if(response.code() == HttpURLConnection.HTTP_OK) {
                    var roomsList: List<Room>? = response.body();
                    roomsView?.value = roomsList!!;
                }
            }

            override fun onFailure(call: Call<List<Room>>, t: Throwable) {

            }
        })
        return roomsView
    }
}
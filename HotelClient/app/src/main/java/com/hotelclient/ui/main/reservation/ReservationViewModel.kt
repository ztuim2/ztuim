package com.hotelclient.ui.main.reservation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hotelclient.MyApplication
import com.hotelclient.api.ReservationInterface
import com.hotelclient.api.RoomInterface
import com.hotelclient.models.ReservationView
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.rooms.RoomsFragment
import com.hotelclient.utillities.TokenExplorator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.net.HttpURLConnection
import javax.inject.Inject

class ReservationViewModel(private val mApplication: ReservationFragment) : ViewModel() {
    var retrofit: Retrofit? = null @Inject set
    var reservationView = MutableLiveData<List<ReservationView>>()

    init {
        MyApplication.appComponent?.inject(this)
        getReservations();
    }

    fun getReservationViews(): MutableLiveData<List<ReservationView>>? {
        return reservationView
    }

    fun getReservations(): MutableLiveData<List<ReservationView>>? {

        val reservationService: ReservationInterface = retrofit!!.create(ReservationInterface::class.java)
        val call: Call<List<ReservationView>> = reservationService.getReservations(TokenExplorator.getUserId(mApplication.requireContext())!!)
        call?.enqueue(object : Callback<List<ReservationView>> {
            override fun onResponse(
                call: Call<List<ReservationView>>,
                response: Response<List<ReservationView>>
            ) {
                var reservationList: List<ReservationView>? = response.body();
                reservationView?.value = reservationList!!;
            }

            override fun onFailure(call: Call<List<ReservationView>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
        return reservationView
    }
}
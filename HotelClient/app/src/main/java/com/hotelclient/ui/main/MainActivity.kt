package com.hotelclient.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.hotelclient.R
import com.hotelclient.ui.auth.AuthActivity
import com.hotelclient.ui.main.home.HomeFragment
import com.hotelclient.utillities.ActivityChanger
import com.hotelclient.utillities.SharedPreferencesManager
import com.hotelclient.utillities.TokenExplorator
import java.util.*


class MainActivity : AppCompatActivity() {

    private var mAppBarConfiguration: AppBarConfiguration? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView>(R.id.main_nav_view)
        navigationView.menu.findItem(R.id.sign_out_button)
            .setOnMenuItemClickListener { e: MenuItem? -> signOut() }
        setUserDetailsInMenu(navigationView)
        mAppBarConfiguration = AppBarConfiguration.Builder(R.id.nav_home, R.id.nav_search, R.id.nav_reservations)
            .setDrawerLayout(drawer)
            .build()

        val navController = Navigation.findNavController(this, R.id.main_nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration!!)
        NavigationUI.setupWithNavController(navigationView, navController)
        Objects.requireNonNull(supportActionBar)!!.setDisplayShowTitleEnabled(false)
    }

    private fun signOut(): Boolean {
        SharedPreferencesManager.clearSharedPreferences(applicationContext)
        ActivityChanger.change(this, AuthActivity::class.java)
        Toast.makeText(this, getString(R.string.successful_sign_out), Toast.LENGTH_SHORT).show()
        finish()
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.main_nav_host_fragment)
        return (NavigationUI.navigateUp(navController, mAppBarConfiguration!!)
                || super.onSupportNavigateUp())
    }

    override fun onBackPressed() {
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START) else super.onBackPressed()
    }

    private fun setUserDetailsInMenu(navigationView: NavigationView) {
        val navHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main)
        val userName = navHeaderView.findViewById<TextView>(R.id.menu_user_name)
        userName.setText(TokenExplorator.getUserNameFromToken(applicationContext))
    }
}
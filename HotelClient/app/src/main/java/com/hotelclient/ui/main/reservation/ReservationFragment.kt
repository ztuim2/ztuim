package com.hotelclient.ui.main.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.hotelclient.R
import com.hotelclient.models.ReservationView
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.rooms.RoomsAdapter
import com.hotelclient.ui.main.rooms.RoomsViewModel
import com.hotelclient.ui.main.rooms.RoomsViewModelFactory
import com.hotelclient.ui.main.search.SearchViewModel
import java.util.ArrayList

class ReservationFragment : Fragment() {
    private var reservations: List<ReservationView> = ArrayList<ReservationView>()
    private var mRecyclerView: RecyclerView? = null
    private var reservationAdapter: ReservationAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_reservations, container, false)
        val mViewModel: ReservationViewModel by viewModels { ReservationViewModelFactory(this) }
        mRecyclerView = view.findViewById(R.id.reservationRecyclerView)
        reservationAdapter = ReservationAdapter(reservations, activity!!)
        mRecyclerView?.setAdapter(reservationAdapter)

        mViewModel.getReservationViews()?.observe(viewLifecycleOwner) { reservationViews ->
            reservations = reservationViews
            reservationAdapter = ReservationAdapter(reservations, activity!!)
            mRecyclerView?.setAdapter(reservationAdapter)
        }
        return view
    }
}
package com.hotelclient.ui.main.rooms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hotelclient.models.SearchDate


class RoomsViewModelFactory(private val mApplication: RoomsFragment, private val mParam: SearchDate?) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RoomsViewModel(mApplication, mParam) as T
    }
}
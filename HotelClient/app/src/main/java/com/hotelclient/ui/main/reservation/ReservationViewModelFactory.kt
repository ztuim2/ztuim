package com.hotelclient.ui.main.reservation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.rooms.RoomsFragment
import com.hotelclient.ui.main.rooms.RoomsViewModel

class ReservationViewModelFactory(private val mApplication: ReservationFragment) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReservationViewModel(mApplication) as T
    }
}
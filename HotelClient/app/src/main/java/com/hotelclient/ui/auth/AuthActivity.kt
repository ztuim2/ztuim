package com.hotelclient.ui.auth

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.hotelclient.R
import com.hotelclient.utillities.SharedPreferencesManager
import com.hotelclient.utillities.TokenExplorator
import java.util.*

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        Objects.requireNonNull(supportActionBar)!!.setDisplayShowTitleEnabled(false)

        val mAppBarConfiguration = AppBarConfiguration.Builder(R.id.nav_login, R.id.nav_register)
            .build()

        val navigationView = findViewById<NavigationView>(R.id.auth_nav_view)

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_auth)
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration)
        NavigationUI.setupWithNavController(navigationView, navController)

        val context = applicationContext

        if (SharedPreferencesManager.containsToken(context)) {
            if (TokenExplorator.isTokenExpired(context)) {
                removeToken(context)
            } else {
                redirectToMainApp()
            }
        }
    }

    private fun removeToken(context: Context) {
        Toast.makeText(context, getString(R.string.token_expired_toast), Toast.LENGTH_SHORT).show()
        SharedPreferencesManager.clearSharedPreferences(context)
    }

    private fun redirectToMainApp() {
        //ActivityChanger.change(this, MainActivity::class.java)
        //finish()
    }
}
package com.hotelclient.ui.main.reservation

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hotelclient.R
import com.paypal.checkout.paymentbutton.PayPalButton

class ReservationViewHolder(view: View): RecyclerView.ViewHolder(view) {
    var roomName: TextView? = null
    var roomPrice: TextView? = null
    var dateFrom: TextView? = null
    var dateTo: TextView? = null
    var payStatus: TextView? = null
    var deleteButton: Button? = null
    var payButton: PayPalButton? = null

    init {
        roomName = itemView.findViewById(R.id.roomName)
        roomPrice = itemView.findViewById(R.id.price)
        dateFrom = itemView.findViewById(R.id.dateFrom)
        dateTo = itemView.findViewById(R.id.dateTo)
        payStatus = itemView.findViewById(R.id.payStatus)
        deleteButton = itemView.findViewById(R.id.deleteButton)
        payButton = itemView.findViewById(R.id.payPalButton)
    }
}
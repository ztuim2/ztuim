package com.hotelclient.ui.main.search

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import com.hotelclient.MyApplication
import com.hotelclient.R
import com.hotelclient.models.SearchDate
import com.hotelclient.ui.main.rooms.RoomsFragment
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject


class SearchFragment : Fragment() {
    var picker: DatePickerDialog? = null
    var dateFrom: EditText? = null
    var dateTo: EditText? = null
    var retrofit: Retrofit? = null @Inject set
    private lateinit var searchViewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchViewModel = activity?.run {
            ViewModelProviders.of(this).get(SearchViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        MyApplication.appComponent?.inject(this)
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        dateFrom = view.findViewById(R.id.editText1) as EditText?
        dateTo = view.findViewById(R.id.editText2) as EditText?
        dateFrom!!.inputType = InputType.TYPE_NULL
        dateTo!!.inputType = InputType.TYPE_NULL
        dateFrom!!.setOnClickListener {
            val cldr: Calendar = Calendar.getInstance()
            val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
            val month: Int = cldr.get(Calendar.MONTH)
            val year: Int = cldr.get(Calendar.YEAR)
            picker = DatePickerDialog(
                view.context,
                { view, year, monthOfYear, dayOfMonth -> dateFrom!!.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year) },
                year,
                month,
                day
            )
            picker!!.show()
        }

        dateTo!!.setOnClickListener {
            val cldr: Calendar = Calendar.getInstance()
            val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
            val month: Int = cldr.get(Calendar.MONTH)
            val year: Int = cldr.get(Calendar.YEAR)
            picker = DatePickerDialog(
                view.context,
                { view, year, monthOfYear, dayOfMonth -> dateTo!!.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year) },
                year,
                month,
                day
            )
            picker!!.show()
        }

        val signInButton = view.findViewById<Button>(R.id.searchButton)
        signInButton.setOnClickListener { e: View? -> search() }
        return view
    }

    private fun search() {
        val searchDate: SearchDate = SearchDate(dateFrom?.text.toString(),dateTo?.text.toString())
        searchViewModel.searchDate.value = searchDate
        val fragmentTransaction: FragmentTransaction = activity?.supportFragmentManager!!.beginTransaction()
        fragmentTransaction.replace(R.id.main_nav_host_fragment, RoomsFragment()).addToBackStack(null)
        fragmentTransaction.commit()
    }
}
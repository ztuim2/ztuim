package com.hotelclient.di.components

import com.hotelclient.di.modules.RetrofitModule
import com.hotelclient.ui.auth.LoginFragment
import com.hotelclient.ui.auth.RegisterFragment
import com.hotelclient.ui.main.reservation.ReservationAdapter
import com.hotelclient.ui.main.reservation.ReservationViewModel
import com.hotelclient.ui.main.rooms.RoomsAdapter
import com.hotelclient.ui.main.rooms.RoomsViewModel
import com.hotelclient.ui.main.search.SearchFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitModule::class])
interface AppComponent {
    fun inject(loginFragment: LoginFragment?)

    fun inject(registerFragment: RegisterFragment?)

    fun inject(searchFragment: SearchFragment?)

    fun inject(roomsViewModel: RoomsViewModel?)

    fun inject(roomsAdapter: RoomsAdapter)

    fun inject(reservationViewModel: ReservationViewModel)

    fun inject(reservationAdapter: ReservationAdapter)
}
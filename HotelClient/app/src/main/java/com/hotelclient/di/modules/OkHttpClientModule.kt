package com.hotelclient.di.modules

import android.content.Context
import com.hotelclient.utillities.SharedPreferencesManager
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class OkHttpClientModule(private val context: Context) {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okBuilder = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor { chain: Interceptor.Chain ->
                var request = chain.request()
                if (SharedPreferencesManager.containsToken(context)) {
                    request = request.newBuilder()
                        .addHeader("Authorization", SharedPreferencesManager.getToken(context).toString())
                        .build()
                }
                chain.proceed(request)
            }
        return okBuilder.build()
    }
}
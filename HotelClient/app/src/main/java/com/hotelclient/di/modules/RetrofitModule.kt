package com.hotelclient.di.modules

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Module(includes = [GsonModule::class, OkHttpClientModule::class])
class RetrofitModule(private val urlPath: String) {
    @Inject
    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson?, client: OkHttpClient?): Retrofit {
        return Retrofit.Builder()
            .baseUrl(urlPath)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
    }
}

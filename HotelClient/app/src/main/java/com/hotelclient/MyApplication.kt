package com.hotelclient

import android.R
import android.app.Application
import com.hotelclient.di.components.AppComponent
import com.hotelclient.di.components.DaggerAppComponent
import com.hotelclient.di.modules.GsonModule
import com.hotelclient.di.modules.OkHttpClientModule
import com.hotelclient.di.modules.RetrofitModule
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.createorder.CurrencyCode
import org.apache.http.conn.ssl.SSLSocketFactory
import java.io.IOException
import java.security.KeyManagementException
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


class MyApplication : Application() {
    private var YOUR_CLIENT_ID = "AUW6HzGZWjnBLnJQC46G0XBSle2VEMB7bxfCKfK3Zi4nMUoo2btfs1tvgBIJpStgONQJ8box6ICA50vH"
    override fun onCreate() {
        super.onCreate()
        val config = CheckoutConfig(
            application = this,
            clientId = YOUR_CLIENT_ID,
            environment = Environment.SANDBOX,
            returnUrl = "com.hotelclient://paypalpay",
            currencyCode = CurrencyCode.PLN,
            userAction = UserAction.PAY_NOW,
            settingsConfig = SettingsConfig(
                loggingEnabled = true
                )
        )
        PayPalCheckout.setConfig(config)
        appComponent = DaggerAppComponent.builder()
            .retrofitModule(RetrofitModule("http://10.0.2.2:5000/"))
            .gsonModule(GsonModule())
            .okHttpClientModule(OkHttpClientModule(applicationContext))
            .build()
    }

    companion object {
        var appComponent: AppComponent? = null
    }
}
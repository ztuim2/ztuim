package com.hotelclient.utillities

import android.content.Context
import android.content.Intent

object ActivityChanger {
    fun change(context: Context, activity: Class<*>?) {
        context.startActivity(Intent(context, activity))
    }
}

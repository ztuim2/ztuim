package com.hotelclient.utillities

import android.content.Context
import com.auth0.android.jwt.JWT
import com.hotelclient.ui.main.rooms.RoomsAdapter
import org.apache.commons.lang3.StringUtils
import java.util.*

object TokenExplorator {
    fun getUserNameFromToken(context: Context): String {
        val jwt: JWT = getJWT(context)
        val userName: String? = jwt.getClaim("unique_name").asString()
        return StringUtils.capitalize(userName)
    }

    fun getUserEmailFromToken(context: Context): String? {
        val jwt: JWT = getJWT(context)
        return Objects.requireNonNull(jwt.getSubject())
    }

    fun isTokenExpired(context: Context): Boolean {
        return getJWT(context).isExpired(10)
    }

    fun getUserId(context: Context): String? {
        val jwt: JWT = getJWT(context)
        return jwt.getClaim("sub").asString()
    }

    private fun getJWT(context: Context): JWT {
        val token = SharedPreferencesManager.getToken(context)
        return JWT(token!!)
    }
}
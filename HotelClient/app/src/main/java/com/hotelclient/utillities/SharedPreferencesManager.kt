package com.hotelclient.utillities

import android.content.Context

object SharedPreferencesManager {
    private const val pref = "Authorization"
    fun clearSharedPreferences(context: Context) {
        context.getSharedPreferences(pref, Context.MODE_PRIVATE).edit().clear().apply()
    }

    fun containsToken(context: Context): Boolean {
        val sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE)
        return sharedPreferences.contains(pref)
    }

    fun saveToken(context: Context, token: String?) {
        val sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(pref, token)
        editor.apply()
    }

    fun getToken(context: Context): String? {
        val sharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE)
        return sharedPreferences.getString(pref, null)
    }
}
package com.hotelclient.models

data class User(val email: String, val password: String)

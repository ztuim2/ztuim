package com.hotelclient.models

data class ReservationView(
    var id: String,
    var description: String,
    var clientId: String,
    var clientName: String,
    var price: Number,
    var start: String,
    var end: String,
    var reservationStatus: String,
    var payStatus: String,
    var remarks: String,
    var rooms: List<Room>
)

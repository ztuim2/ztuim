package com.hotelclient.models

import java.util.*

data class Reservation(
    var id: String,
    var description: String,
    var clientId: String,
    var start: String,
    var end: String,
    var remarks: String,
    var roomIds: List<String>
)

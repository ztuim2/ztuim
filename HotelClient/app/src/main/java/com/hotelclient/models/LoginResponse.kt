package com.hotelclient.models

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    var token: String
)

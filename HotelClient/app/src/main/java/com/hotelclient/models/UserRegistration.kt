package com.hotelclient.models

data class UserRegistration(val firstName: String, val lastName:String, val phoneNumber: String, val email: String, val password: String )

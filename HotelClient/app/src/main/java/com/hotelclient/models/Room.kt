package com.hotelclient.models

data class Room(val id:String,val number:Number,val price:Number,val type:String)

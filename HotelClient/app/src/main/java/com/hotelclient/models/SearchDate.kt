package com.hotelclient.models

import java.time.LocalDateTime
import java.util.*

data class SearchDate(
    var start: String,
    var end: String
)

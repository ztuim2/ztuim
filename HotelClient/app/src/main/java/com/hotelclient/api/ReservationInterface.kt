package com.hotelclient.api

import com.hotelclient.models.Reservation
import com.hotelclient.models.ReservationView
import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import retrofit2.Call
import retrofit2.http.*

interface ReservationInterface {

    @POST("/api/Reservation")
    fun reserve(@Body reservation: Reservation): Call<Void>

    @GET("/api/Reservation/client/{clientId}")
    fun getReservations(@Path("clientId") clientId: String): Call<List<ReservationView>>

    @PUT("/api/Reservation/pay/{id}")
    fun payReservation(@Path("id") id: String): Call<Void>

    @DELETE("/api/Reservation/{id}")
    fun deleteReservation(@Path("id") id: String): Call<Void>
}
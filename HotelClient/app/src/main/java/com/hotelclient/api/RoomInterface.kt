package com.hotelclient.api

import com.hotelclient.models.Room
import com.hotelclient.models.SearchDate
import com.hotelclient.models.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface RoomInterface {
    @POST("/api/Room/available")
    fun getAvailableRooms(@Body searchDate: SearchDate?): Call<List<Room>>
}
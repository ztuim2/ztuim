import com.hotelclient.models.LoginResponse
import com.hotelclient.models.User
import com.hotelclient.models.UserRegistration
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserInterface {
    @POST("api/Client")
    fun signUp(@Body user: UserRegistration): Call<Void>

    @POST("api/Auth/login")
    fun signIn(@Body user: User): Call<String>
}